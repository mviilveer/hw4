import java.math.BigInteger;
import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction a = Lfraction.valueOf ("1/2");
      System.out.println(a.hashCode());
      a = Lfraction.valueOf ("2/1");
      System.out.println(a.hashCode());
      //System.out.println(a.getDenominator());
   }

   private long numerator;

   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      validateAndAssign(a, b);
   }

   private void validateAndAssign(long numerator, long denominator) {
      raiseErrorIfZero(denominator);

      if (denominator < 0) {
         denominator = -1 * denominator;
         numerator = -1 * numerator;
      }

      long gcd = gcd(numerator, denominator);
      this.numerator = numerator / gcd;
      this.denominator = denominator / gcd;
   }

   private long gcd(long a, long b) {
      BigInteger b1 = BigInteger.valueOf(a);
      BigInteger b2 = BigInteger.valueOf(b);
      BigInteger gcd = b1.gcd(b2);
      return gcd.intValue();
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return getNumerator() + "/" + getDenominator();
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      Lfraction object = (Lfraction)m;
      return object.getDenominator() == getDenominator() && object.getNumerator() == getNumerator();
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      // http://stackoverflow.com/questions/2738886/what-is-a-best-practice-of-writing-hash-function-in-java
      return Arrays.hashCode(new Object[] {
           denominator,
           numerator,
      });
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {

      long numerator = getNumerator() * m.getDenominator() + m.getNumerator() * getDenominator();
      long denominator = getDenominator() * m.getDenominator();
      return new Lfraction(numerator, denominator);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      return new Lfraction (getNumerator() * m.getNumerator(), getDenominator() * m.getDenominator());
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      raiseErrorIfZero(getNumerator());
      return new Lfraction(getDenominator(), getNumerator());
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(getNumerator(), getDenominator() * -1);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      long newNumerator = (getNumerator() * m.getDenominator()) - (m.getNumerator() * getDenominator());
      return new Lfraction (newNumerator, getDenominator() * m.getDenominator());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      long newDenominator = getDenominator() * m.getNumerator();
      raiseErrorIfZero(newDenominator);
      return new Lfraction(getNumerator() * m.getDenominator(), newDenominator);
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      long numerator = getNumerator() * m.getDenominator();
      long comparableNumerator = m.getNumerator() * getDenominator();

      if (numerator < comparableNumerator) {
         return -1;
      } else if (numerator > comparableNumerator) {
         return 1;
      } else {
         return 0;
      }
   }

   private void raiseErrorIfZero(double denominator)
   {
      if (denominator == 0) {
         throw new ArithmeticException("Division by zero.");
      }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(getNumerator(), getDenominator());
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return (long)toDouble();
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      long numerator = getNumerator() / getDenominator();
      if (numerator == 0) {
         numerator = getNumerator();
      } else {
         numerator = getNumerator() % getDenominator();
      }
      return new Lfraction(numerator, getDenominator());
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (float)getNumerator() / (float)getDenominator();
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction(Math.round(f * d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {

      String[] parts = s.trim().split("/");
      if (parts.length != 2) {
         throw new RuntimeException("Invalid string to parse: '" + s.trim() + "'.");
      }
      Long[] doubles = new Long[parts.length];

      for (int i = 0; i < parts.length; i++) {
         try {
            doubles[i] = Long.valueOf(parts[i].trim());
         } catch (NumberFormatException e) {
            throw new RuntimeException("Invalid number '" + parts[i].trim() + "'. String to parse: '" + s.trim() + "'.");
         }
      }

      return new Lfraction(doubles[0], doubles[1]);
   }
}

